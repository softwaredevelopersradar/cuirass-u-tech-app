﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using FtmDeviceMessages;

namespace CuirassTechApp
{
    public struct Signal
    {
        public int FrequencyMHz { get; private set; }
        public int Amplitude { get; private set; }
        public float Direction { get; private set; }
        public byte BandwidthMHz { get; private set; }
        public int Duration { get; private set; }
        public int RepeatPeriod { get; private set; }
        public int Hours { get; private set; }
        public int Minutes { get; private set; }
        public int Seconds { get; private set; }
        public int Ticks { get; private set; }
        
        public Signal(byte[] data, int index = 0)
        {
            try
            {

                Direction = BitConverter.ToSingle(data, index);
                var msg = CalculatorResponseMessage.Parse(data, 4 + index);

                var bitFreqAndAmp = Convert.ToString(msg.FrequencyAndAmplitude, 2);
                while (bitFreqAndAmp.Length < 16)
                {
                    bitFreqAndAmp = "0" + bitFreqAndAmp; //we need to add zeroes that were removed as non-informative, because bits bits bits ._.
                }
                var bitFreq = bitFreqAndAmp.Remove(9);
                var bitAmp = bitFreqAndAmp.Remove(0, 9);
                FrequencyMHz = Convert.ToInt32(bitFreq, 2) + 1100;
                Amplitude = Convert.ToInt32(bitAmp, 2);

                var bitApp = Convert.ToString(msg.ImpulseAppearanceStamp, 2);
                
                while (bitApp.Length < 32)
                {
                    bitApp = "0" + bitApp; //we need to add zeroes that were removed as non-informative, because bits bits bits ._.
                }

                var bitSec = bitApp.Remove(6);
                var bitTime = bitApp.Remove(0, 6);

                Hours = msg.Hours;
                Minutes = msg.Minutes;
                Seconds = Convert.ToInt32(bitSec, 2);
                Ticks = Convert.ToInt32(bitTime, 2);

                Duration = Utilities.ThreeBytesToInt(msg.ImpulseDurationLow, msg.ImpulseDurationMiddle,
                               msg.ImpulseDurationHigh) * 5;
                RepeatPeriod = msg.ImpulseRepeatCount * 5;
                BandwidthMHz = msg.BandwidthMhz;
                return;
            }
            catch
            {
                Direction = 0;
                Seconds = 0;
                Ticks = 0;
                Hours = 0;
                Minutes = 0;
                RepeatPeriod = 0;
                BandwidthMHz = 0;
                Amplitude = 0;
                FrequencyMHz = 0;
                Duration = 0;
            }
        }
    }

    public struct IqScan
    {
        public double[] Amplitudes { get; private set; }
        public double[] Phases { get; private set; }

        public IqScan(byte[] iqBytes)
        {
            Amplitudes = new double[256];
            Phases = new double[256];

            if (iqBytes.Length != 1)
            {
                for (int i = 0; i < Amplitudes.Length; i++)
                {
                    var realBytes = new byte[] { iqBytes[4 * i + 1], iqBytes[4 * i]};
                    var imgBytes = new byte[] { iqBytes[4 * i + 3], iqBytes[4 * i + 2] };
                    var real = BitConverter.ToInt16(realBytes, 0);
                    var imaginary = BitConverter.ToInt16(imgBytes, 0);
                    Amplitudes[i] = Math.Sqrt(real * real + imaginary * imaginary);
                    Phases[i] = Math.Atan(imaginary * 1.0d / real);
                }
            }
        }
    }

    public class CuirassClient
    {
        public UdpClient Client { get; private set; }
        public bool IsWorking { get; private set; }
        public bool IsConnected { get; private set; } = false;
        public int UpdateDelayMs { get; set; }

        public event EventHandler<Scan> FftDataReceived;

        public event EventHandler<List<Signal>> SignalsReceived;
        public event EventHandler<IqScan> IqBytesReceived;

        public CuirassClient(UdpClient client, bool isWorking, int updateDelayMs)
        {
            Client = client;
            IsWorking = isWorking;
            UpdateDelayMs = updateDelayMs;
        }

        public void Connect(string ip, int port)
        {
            Client.Connect(ip, port);
        }

        public void Start()
        {
            IsConnected = true;
            Task.Run(async () =>
            {
                while (IsConnected)
                {
                    if (IsWorking)
                        try
                        {
                            Client.Client.ReceiveTimeout = 50;

                            var getLastMasterScanRequest = new byte[] { 2 };
                            Client.Send(getLastMasterScanRequest, getLastMasterScanRequest.Length);
                            var lastMasterScanResponse = Client.Receive();

                            var getLastSlaveScanRequest = new byte[] { 3 };
                            Client.Send(getLastSlaveScanRequest, getLastSlaveScanRequest.Length);
                            var getLastSlaveScanResponse = Client.Receive();

                            var getIqScanRequest = new byte[] { 6 };
                            Client.Send(getIqScanRequest, getIqScanRequest.Length);
                            var iqBytes = Client.Receive();
                            IqBytesReceived?.Invoke(this, new IqScan(iqBytes));

                            var getMasterWbThresholdRequest = new byte[] { 4 };
                            Client.Send(getMasterWbThresholdRequest, getMasterWbThresholdRequest.Length);
                            var getMasterWbThresholdResponse = Client.Receive();

                            var getCalculatorResultsRequest = new byte[] { 5 };
                            Client.Send(getCalculatorResultsRequest, getCalculatorResultsRequest.Length);
                            var getCalculatorResultsResponse = Client.Receive();

                            var signals = new List<Signal>();
                            for (int i = 0; i < getCalculatorResultsResponse.Length; i += 4 + CalculatorResponseMessage.BinarySize)
                            {
                                signals.Add(new Signal(getCalculatorResultsResponse, i));
                            }
                            SignalsReceived?.Invoke(this, signals);

                            var mThreshold = new byte[466];
                            for (int i = 0; i < mThreshold.Length; i++)
                            {
                                mThreshold[i] = getMasterWbThresholdResponse[0];
                            }

                            var scan = new Scan(lastMasterScanResponse, getLastSlaveScanResponse, mThreshold);
                            FftDataReceived?.Invoke(this, scan);
                            await Task.Delay(UpdateDelayMs);
                            continue;
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine($"Error {e}");
                            await Task.Delay(50);
                        }
                }
            });
        }

        public void Stop()
        {
            IsConnected = false;
        }
    }

    public struct Scan
    {
        public List<byte[]> Data;
        
        public Scan(params object[] args)
        {
            Data = new List<byte[]>();
            foreach (var arg in args)
            {
                Data.Add((byte[])arg);
            }
        }
    }

    public static class Extensions
    {
        public static byte[] Receive(this UdpClient client)
        {
            try
            {
                var clientRemotePoint = client.Client.RemoteEndPoint.ToString().Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                var ipEndPoint = new IPEndPoint(IPAddress.Parse(clientRemotePoint[0]), Int32.Parse(clientRemotePoint[1]));
                return client.Receive(ref ipEndPoint);
            }
            catch (Exception e)
            {
                return new byte[] { };
            }
        }
    }
}
