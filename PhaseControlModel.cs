﻿using System;
using System.Collections.Generic;

namespace TechAppControlLibrary.Phases
{
    public class PhaseControlModel : IPhaseControlModel
    {
        public int ShadowValuesCount { get; }

        public Queue<double> Values { get; }

        public event EventHandler ValueAddedEvent;

        public PhaseControlModel(int shadowValuesCount)
        {
            ShadowValuesCount = shadowValuesCount;
            Values = new Queue<double>();
        }

        public void AddValue(double value, bool fireEvent = true)
        {
            Values.Enqueue(value);
            if (Values.Count > ShadowValuesCount)
                Values.Dequeue();

            if(fireEvent)
                ValueAddedEvent?.Invoke(this,EventArgs.Empty);
        }
    }
    public interface IPhaseControlModel
    {
        event EventHandler ValueAddedEvent;

        int ShadowValuesCount { get; }
        Queue<double> Values { get; }

        void AddValue(double value, bool fireEvent = true);
    }
}
