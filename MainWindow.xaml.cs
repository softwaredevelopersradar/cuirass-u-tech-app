﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;
using Arction.Wpf.Charting.SeriesXY;
using Simulator;
using TechAppControlLibrary.Phases;

namespace CuirassTechApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CuirassClient _client;
        private Vm vm;
        private PhaseControl _phaseControl;
        public MainWindow()
        {
            var deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
            
            InitializeComponent();

            _phaseControl = new PhaseControl(115)
            {
                BackgroundColor = Colors.DeepSkyBlue,
                VectorColor = Colors.Orange
            };
            phaseGrid.Children.Add(_phaseControl);


            vm = Dg.DataContext as Vm;

            _client = new CuirassClient(new UdpClient(14881), true, vm.Delay);
            tb.KeyDown += (sender, args) =>
            {
                if (args.Key == Key.Enter)
                    buttonConnect.Focus();
            };
            tb_Copy.KeyDown += (sender, args) =>
            {
                if (args.Key == Key.Enter)
                    buttonConnect.Focus();
            };
            vm.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals("Delay"))
                    _client.UpdateDelayMs = vm.Delay;
            };
            _client.Connect("127.0.0.1", 14880);
            _client.FftDataReceived += (sender, args) => { Dispatcher?.Invoke(() => ProcessScans(args)); };
            _client.IqBytesReceived += (sender, scan) =>
            {
                Dispatcher?.Invoke(() =>
                {
                    try
                    {
                        chart.BeginUpdate();
                        GetData(scan.Amplitudes ,4, chart);
                        GetData(scan.Phases, 5, chart);
                        chart.EndUpdate();
                    }
                    catch
                    {
                    }
                });
            }; 
            _client.SignalsReceived += (sender, list) =>
            {
                Dispatcher?.Invoke(() =>
                {
                    try
                    {
                        vm.Signals.Clear();
                        foreach (var signal in list)
                        {
                            vm.Signals.Add(signal);
                        }

                        if(list.Count != 0)
                            if(vm.DirectionIndex < list.Count)
                                _phaseControl.AddValue(list[vm.DirectionIndex].Direction);
                            else
                                _phaseControl.AddValue(list[0].Direction);
                    }
                    catch
                    {
                    }

                });
            }; 
            var maxPoints = 512;

            var _chart = chart;
            AxisX axisX = _chart.ViewXY.XAxes[0];
            axisX.SetRange(0, maxPoints);
            chart.ViewXY.YAxes[0].ZoomingEnabled = false;
            chart.Title.Visible = false;
            chart.ViewXY.YAxes[0].Title.Visible = false;
            chart.ViewXY.XAxes[0].Title.Visible = false;
            chart.ViewXY.BeforeZooming += (sender, args) =>
            {
                args.Cancel = true;//this is custom zooming via Axis.SetRange(min, max), so lc zooming is disabled
                var newMin = args.XRanges[0].NewMin;
                if (newMin < 0)
                    newMin = 0;
                var newMax = args.XRanges[0].NewMax;
                if (newMax > maxPoints)//to constants
                    newMax = maxPoints;
                args.XRanges[0].Axis.SetRange(newMin, newMax);
            };
            chart.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.Pan;
            chart.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.Zoom;
            axisX.ScrollMode = XAxisScrollMode.None;
            axisX.ValueType = AxisValueType.Number;
            axisX.Maximum = maxPoints;
            _chart.ViewXY.YAxes[0].SetRange(-20, 120);
            for (int i = 0; i < 6; i++)
            {
                SampleDataSeries pls; pls = new SampleDataSeries(_chart.ViewXY, _chart.ViewXY.XAxes[0], _chart.ViewXY.YAxes[0]);
                pls.LineStyle = new LineStyle();
                pls.PointsVisible = false;
                pls.SamplesDouble = new double[1024];
                _chart.ViewXY.SampleDataSeries.Add(pls);
            }

            _client.Start();
        }

        private void ProcessScans(Scan scan)
        {
            chart.BeginUpdate();
            for (int i = 0; i < scan.Data.Count; i++)
            {
                GetData(scan.Data[i], i, chart);
            }
            chart.EndUpdate();
        }

        private void GetData(byte[] scan, int index, LightningChartUltimate chart)
        {
            GetData(scan.Select(b => (double)b).ToArray(), index, chart);
        }

        private void GetData(double[] scan, int index, LightningChartUltimate chart)
        {
            var pls = chart.ViewXY.SampleDataSeries[index];
            for (int pointIndex = 0; pointIndex < scan.Length; pointIndex++)
            {
                pls.SamplesDouble[pointIndex] = scan[pointIndex];
            }
            var ls = pls.LineStyle;
            switch (index)
            {
                case 0:
                    {
                        ls.Color = Color.FromRgb(255, 0, 0);
                        break;
                    }
                case 1:
                    {
                        ls.Color = Color.FromRgb(0, 255, 0);
                        break;
                    }
                case 2:
                    {
                        ls.Color = Color.FromRgb(0, 0, 255);
                        break;
                    }
                case 3:
                    {
                        ls.Color = Color.FromRgb(0, 255, 255);
                        break;
                    }
                case 4:
                    {
                        ls.Color = Color.FromRgb(255, 0, 255);
                        break;
                    }
                case 5:
                    {
                        ls.Color = Color.FromRgb(255, 255, 0);
                        break;
                    }
            }
        }

        private void Connection_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!_client.IsConnected)
            {
                _client.Start();
                buttonConnect.Content = "Stop";
            }
            else
            {
                _client.Stop();
                buttonConnect.Content = "Start";
            }
        }
    }

    public static class Utilities
    {
        public static int ThreeBytesToInt(byte low, byte middle, byte high)
        {
            var bytes = new byte[] { low, middle, high, 0 };
            return BitConverter.ToInt32(bytes, 0);
        }

        public static int ThreeBytesToInt(byte[] data, int index)
        {
            return ThreeBytesToInt(data[index], data[index + 1], data[index + 2]);
        }

        public static short TwoBytesToShort(byte low, byte high)
        {
            var bytes = new byte[] { low, high };
            return BitConverter.ToInt16(bytes, 0);
        }

        public static short TwoBytesToShort(byte[] data, int index)
        {
            return TwoBytesToShort(data[index], data[index + 1]);
        }

        /// <summary>
        /// Converts integer to three bytes, fourth byte is dropped
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static byte[] IntToThreeBytes(int number)
        {
            var bytes = BitConverter.GetBytes(number);
            return new byte[] { bytes[0], bytes[1], bytes[2], 0 };//dropping last byte
        }
    }
}
