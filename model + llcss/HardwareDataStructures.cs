﻿using System;
using System.Collections;

namespace DspDataModel.Hardware
{
    public enum FtmDeviceOutputMode
    {
        NoOutput = 0,
        WideCalculator = 1,
        NarrowCalculator = 2,
        WideAndNarrow = 3,
        IqComponents = 4,
        WideAndIq = 5,
        NarrowAndIq = 6,
        WideAndNarrowAndIq = 7
    }

    public enum SecondsMark
    {
        InternalMark = 0,
        ExternalMark = 1
    }

    public enum GatingMode
    {
        InternalMark = 0,
        ExternalMark = 1
    }

    public enum NarrowBandFilterWidth
    {
        HalfMHz = 0,
        OneMHz = 1,
        TwoMHz = 2,
        FiveMHz = 3,
        TenMHz = 4,
        TwentyMHz = 5,
        FiftyMHz = 6
    }

    public enum WindowFunctionType
    {
        ChebyshevWindow = 0,
        RectangleWindow = 1
    }

    public enum DacVideoMode
    {
        Disabled = 0,
        WideBandDetector = 1,
        NarrowBandDetector = 2
    }

    public enum DacAudioMode
    {
        Disabled = 0,
        WideBandDetector = 1,
        NarrowBandDetector = 2,
        //todo : add and deal with misunderstandings
    }

    public enum Gps2DIndicationModes
    {
        Pass = 0,
        _2D = 1,
        OffAnt = 2
    }

    public struct SelectionSetting
    {
        public readonly int MinImpulses;
        public readonly int MaxImpluses;
        public readonly bool IsActive;

        public SelectionSetting(int minImpulses, int maxImpulses, bool isActive)
        {
            MinImpulses = minImpulses;
            MaxImpluses = maxImpulses;
            IsActive = isActive;
        }
    }
}
