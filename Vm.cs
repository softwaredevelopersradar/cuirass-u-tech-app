﻿using CuirassTechApp;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Simulator
{
    public class Vm : INotifyPropertyChanged
    {
        public ObservableCollection<Signal> Signals { get; set; }

        private int _delay = 40;

        public int Delay
        {
            get => _delay;
            set
            {
                if (value >= 25 && value <= 1000)
                {
                    _delay = value;
                    OnPropertyChanged(nameof(Delay));
                }
            }
        }

        private int _directionIndex = 0;
        public int DirectionIndex
        {
            get => _directionIndex;
            set
            {
                if (value >= 0)
                {
                    _directionIndex = value;
                    OnPropertyChanged(nameof(DirectionIndex));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Vm()
        {
            Signals = new ObservableCollection<Signal>();
            OnPropertyChanged(nameof(Signals));
            OnPropertyChanged(nameof(Delay));
            OnPropertyChanged(nameof(DirectionIndex));
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}