﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Annotations;
using Arction.Wpf.Charting.SeriesXY;

namespace TechAppControlLibrary.Phases
{
    /// <summary>
    /// Interaction logic for PhaseControl.xaml
    /// </summary>
    public partial class PhaseControl : UserControl
    {
        private IPhaseControlModel _model;
        private LightningChartUltimate _chart;
        private readonly int _size;
        private Color _backGroundColor;

        public Color VectorColor { get; set; }
        public Color BackgroundColor
        {
            get => _backGroundColor;
            set
            {
                _backGroundColor = value;
                _chart.ViewXY.PointLineSeries[0].PointStyle.BorderColor = value;
                _chart.ViewXY.PointLineSeries[0].PointStyle.Color1 = value;
            }
        }

        public PhaseControl()
        {
            var deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
            InitializeComponent();

            VectorColor = Colors.Orange;
            BackgroundColor = Colors.DeepSkyBlue;
            _size = 100;
            Initialization();
        }

        public PhaseControl(int size)
        {
            InitializeComponent();
            _size = size;
            Initialization();
        }

        public void AddValue(double value) => _model.AddValue(value);

        private void Initialization()
        {
            _chart = new LightningChartUltimate();
            _model = new PhaseControlModel(20);
            _model.ValueAddedEvent += (sender, args) => { ShowData(); };
            Grid.Children.Add(_chart);
            _chart.BeginUpdate();
            _chart.Title.Visible = false;
            _chart.ViewXY.LegendBoxes = null;
            _chart.ViewXY.XAxes[0].Visible = false;
            _chart.ViewXY.YAxes[0].Visible = false;

            _chart.Width = _size;
            _chart.Height = _size;
            _chart.MaxHeight = _size;
            _chart.MinHeight = _size;
            _chart.MinWidth = _size;
            _chart.MaxWidth = _size;
            //adding angle annotation

            var angleAnnotation = new AnnotationXY(
                owner: _chart.ViewXY,
                axisX: _chart.ViewXY.XAxes[0],
                axisY: _chart.ViewXY.YAxes[0])
            {
                LocationCoordinateSystem = CoordinateSystem.AxisValues,
                Style = AnnotationStyle.Arrow,
                MouseInteraction = false,
                Tag = "angleAnnotation"
            };
            angleAnnotation.ArrowLineStyle.Color = VectorColor;
            var coord = Math.Max(_size / 11, 6);
            angleAnnotation.LocationAxisValues.SetValues(coord, coord);
            angleAnnotation.TargetAxisValues.SetValues(coord, coord);
            angleAnnotation.ArrowLineStyle.Color = Colors.Transparent;
            angleAnnotation.TextStyle.Font.Size = 10;
            angleAnnotation.TextStyle.Color = Colors.LightGreen;
            angleAnnotation.Text = "";
            var nameAnnotation = new AnnotationXY(
                owner: _chart.ViewXY,
                axisX: _chart.ViewXY.XAxes[0],
                axisY: _chart.ViewXY.YAxes[0])
            {
                LocationCoordinateSystem = CoordinateSystem.AxisValues,
                Style = AnnotationStyle.Arrow,
                MouseInteraction = false,
                Tag = "nameAnnotation"
            };
            nameAnnotation.ArrowLineStyle.Color = VectorColor;
            nameAnnotation.LocationAxisValues.SetValues(_size - coord, coord);
            nameAnnotation.TargetAxisValues.SetValues(_size - coord, coord);
            nameAnnotation.ArrowLineStyle.Color = Colors.Transparent;
            nameAnnotation.TextStyle.Font.Size = 10;
            nameAnnotation.TextStyle.Color = Colors.LightGreen;
            nameAnnotation.Text = "";


            _chart.ViewXY.Annotations.Add(angleAnnotation);
            _chart.ViewXY.Annotations.Add(nameAnnotation);

            _chart.ViewXY.ZoomPanOptions.AxisMouseWheelAction = AxisMouseWheelAction.None;
            _chart.ViewXY.ZoomPanOptions.MouseWheelZooming = MouseWheelZooming.Off;
            _chart.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.None;
            _chart.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.None;

            _chart.ViewXY.XAxes[0].SetRange(0, _size);
            _chart.ViewXY.YAxes[0].SetRange(0, _size);


            var pls = new PointLineSeries(_chart.ViewXY, _chart.ViewXY.XAxes[0], _chart.ViewXY.YAxes[0]);
            _chart.ViewXY.PointLineSeries.Add(pls);
            pls.Points = new SeriesPoint[1];
            pls.Points[0] = new SeriesPoint(_size / 2, _size / 2);
            pls.PointStyle.BorderColor = BackgroundColor;
            pls.PointStyle.Shape = Shape.Circle;
            pls.PointsVisible = true;
            pls.PointStyle.Color1 = BackgroundColor;
            pls.MouseHighlight = MouseOverHighlight.None;
            pls.PointStyle.BorderWidth = 2;
            var u = pls.PointStyle.BorderWidth * 5;
            pls.PointStyle.Width = _size - u;
            pls.PointStyle.Height = _size - u;

            _chart.EndUpdate();
        }

        private void ShowData()
        {
            _chart.BeginUpdate();
            
            for (int i = 0 ; i < _model.Values.Count; i++)
            {
                AnnotationXY vector;
                try
                {
                    vector = _chart.ViewXY.Annotations[i+2];
                }
                catch
                {
                    vector = new AnnotationXY(_chart.ViewXY, _chart.ViewXY.XAxes[0], _chart.ViewXY.YAxes[0])
                    {
                        Style = AnnotationStyle.Arrow,
                        LocationCoordinateSystem = CoordinateSystem.AxisValues,
                        MouseInteraction = false,
                        ArrowStyleBegin = ArrowStyle.None
                    };
                    _chart.ViewXY.Annotations.Add(vector);
                }
                var center = _size / 2;
                vector.TextStyle.Visible = false;
                vector.LocationAxisValues.X = center;
                vector.LocationAxisValues.Y = center;

                (var x, var y) = AngleAmplitudeToXy(_model.Values.ElementAt(i) + 90, center - 4);
                vector.TargetAxisValues.X = x + center;
                vector.TargetAxisValues.Y = y + center;
                
                vector.ArrowLineStyle.Width = 2;
                
                var baseColor = Color.FromArgb(255, VectorColor.R, VectorColor.G, VectorColor.B);
                var color = baseColor;
                color.A = (byte) ((double) i / (double) (_model.Values.Count - 1) * 100.0);
                vector.ArrowLineStyle.Color = color;
                
                if (i == _model.Values.Count - 1)
                {
                    vector.ArrowLineStyle.Width = 3f;
                    vector.ArrowLineStyle.Color = VectorColor;
                }

                var angleAnnotation = _chart.ViewXY.Annotations[0];
            }
            _chart.EndUpdate();
        }

        private (double angle, double amplitude) XyToAngleAmplitude(double x, double y) =>
            (
            angle: Math.Atan((y * Math.PI / 180) / (x * Math.PI / 180)),
            amplitude: Math.Sqrt(x * x + y * y)
            );

        private (double x, double y) AngleAmplitudeToXy(double angle, double amplitude) =>
            (
            x: amplitude * Math.Cos(angle * Math.PI / 180),
            y: amplitude * Math.Sin(angle * Math.PI / 180)
            );

    }
}
